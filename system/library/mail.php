<?php
/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
* Mail class
*/
class Mail {
	protected $to;
	protected $from;
	protected $sender;
	protected $reply_to;
	protected $subject;
	protected $text;
	protected $html;
	protected $attachments = array();
	public $parameter;

	/**
	 * Constructor
	 *
	 * @param	string	$adaptor
	 *
 	*/
	public function __construct($adaptor = 'mail') {
		$class = 'Mail\\' . $adaptor;

		if (class_exists($class)) {
			$this->adaptor = new $class();
		} else {
			trigger_error('Error: Could not load mail adaptor ' . $adaptor . '!');
			exit();
		}
	}

	/**
     *
     *
     * @param	mixed	$to
     */
	public function setTo($to) {
		$this->to = $to;
	}

	/**
     *
     *
     * @param	string	$from
     */
	public function setFrom($from) {
		$this->from = $from;
	}

	/**
     *
     *
     * @param	string	$sender
     */
	public function setSender($sender) {
		$this->sender = $sender;
	}

	/**
     *
     *
     * @param	string	$reply_to
     */
	public function setReplyTo($reply_to) {
		$this->reply_to = $reply_to;
	}

	/**
     *
     *
     * @param	string	$subject
     */
	public function setSubject($subject) {
		$this->subject = $subject;
	}

	/**
     *
     *
     * @param	string	$text
     */
	public function setText($text) {
		$this->text = $text;
	}

	/**
     *
     *
     * @param	string	$html
     */
	public function setHtml($html) {
		$this->html = $html;
	}

	/**
     *
     *
     * @param	string	$filename
     */
	public function addAttachment($filename) {
		$this->attachments[] = $filename;
	}

	/**
     *
     *
     */
	public function send() {
		if (!$this->to) {
			throw new \Exception('Error: E-Mail to required!');
		}

		if (!$this->from) {
			throw new \Exception('Error: E-Mail from required!');
		}

		if (!$this->sender) {
			throw new \Exception('Error: E-Mail sender required!');
		}

		if (!$this->subject) {
			throw new \Exception('Error: E-Mail subject required!');
		}

		if ((!$this->text) && (!$this->html)) {
			throw new \Exception('Error: E-Mail message required!');
		}
		//original
		/*foreach (get_object_vars($this) as $key => $value) {
			$this->adaptor->$key = $value;
		}

		$this->adaptor->send();*/

		//sendgrid
		if(true) {

			if (is_array($this->to)) {
	            $to = implode(',', $this->to);
	        } else {
	            $to = $this->to;
	        }

	        $textorder = array("\r\n", "\n", "\r", PHP_EOL);

	        $mailtext = str_replace($textorder, "<br/>", $this->text);

	        $message = isset($this->html) ? $this->html : $mailtext;
	        $personalizations = Array(
	            "personalizations" => Array(
	                0 => Array(
	                    "to" => Array(
	                        0 => Array(
	                            "email" => $to
	                        )),
	                    "subject" => $this->subject),
	            ),
	            "from" => Array(
	                "email" => $this->from,
	                "name" => $this->sender
	            ),
	            "reply_to" => Array(
	                "email" => $this->from,
	                "name" => $this->sender
	            ),
	            "subject" => $this->subject,
	            "content" => Array(
	                0 => Array(
	                    "type" => "text/html",
	                    "value" => $message
	                ))
	        );

	        //echo "<pre>";print_r($personalizations);die;
	        $curl = curl_init();

	        curl_setopt_array($curl, array(
	            CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
	            CURLOPT_RETURNTRANSFER => true,
	            CURLOPT_ENCODING => "",
	            CURLOPT_MAXREDIRS => 10,
	            CURLOPT_TIMEOUT => 30,
	            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	            CURLOPT_CUSTOMREQUEST => "POST",
	            CURLOPT_POSTFIELDS => json_encode($personalizations),
	            CURLOPT_HTTPHEADER => array(
	                "authorization: Bearer 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",
	                "content-type: application/json"
	            ),
	        ));
	        
	        $response = curl_exec($curl);

	        
	        $err = curl_error($curl);
	        

	        curl_close($curl);

	        if ($err) {
	            echo "cURL Error #:" . $err;
	        } else {
	            echo $response;
	        }
		}
	}
}