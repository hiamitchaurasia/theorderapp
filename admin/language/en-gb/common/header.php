<?php
// Heading
$_['heading_title']      = 'Kirana Korner';

// Text
$_['text_profile']       = 'Your Profile';
$_['text_store']         = 'Stores';
$_['text_help']          = 'Help';
$_['text_homepage']      = 'Kirana Korner Homepage';
$_['text_support']       = 'Support Forum';
$_['text_documentation'] = 'Documentation';
$_['text_logout']        = 'Logout';