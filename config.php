<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/theorderapp/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/theorderapp/');

// DIR
/* define('DIR_APPLICATION', '/Users/abhishekchaurasia/Sites/projects/theorderapp/catalog/');
define('DIR_SYSTEM', '/Users/abhishekchaurasia/Sites/projects/theorderapp/system/');
define('DIR_IMAGE', '/Users/abhishekchaurasia/Sites/projects/theorderapp/image/');
define('DIR_STORAGE', '/Users/abhishekchaurasia/Sites/storage/'); */
define('DIR_APPLICATION', 'D:/Softwares/Servers/wamp64/www/theorderapp/catalog/');
define('DIR_SYSTEM', 'D:/Softwares/Servers/wamp64/www/theorderapp/system/');
define('DIR_IMAGE', 'D:/Softwares/Servers/wamp64/www/theorderapp/image/');
define('DIR_STORAGE', 'D:/Softwares/Servers/wamp64/opencartstorage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'goldtree9');
define('DB_DATABASE', 'theorderapp');
define('DB_PORT', '3308');
define('DB_PREFIX', 'oc_');