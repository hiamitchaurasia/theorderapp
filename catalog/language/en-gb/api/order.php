<?php
// Text
$_['text_success']           = 'Success: You have modified orders!';

// Error
$_['error_permission']       = 'You do not have permission to access the API!';
$_['error_customer']         = 'Customer details needs to be set!';
$_['error_payment_address']  = 'Payment address required!';
$_['error_payment_method']   = 'Payment method required!';
$_['error_no_payment']       = 'No Payment options are available!';
$_['error_shipping_address'] = 'Shipping address required!';
$_['error_shipping_method']  = 'Shipping method required!';
$_['error_no_shipping']      = 'No Shipping options are available!';
$_['error_stock']            = 'Products marked with *** are not available in the desired quantity or not in stock!';
$_['error_minimum']          = 'Minimum order amount for %s is %s!';
$_['error_not_found']        = 'Order could not be found!';