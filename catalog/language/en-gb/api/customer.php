<?php
// Text
$_['text_success']       = 'You have successfully modified customers';

// Error
$_['error_permission']   = 'You do not have permission to access the API!';
$_['error_customer']     = 'You must select a customer!';
$_['error_firstname']    = 'Name must be between 1 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 1 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']    = 'Mobile number should be a valid Indian number of 10 digits!';
$_['error_custom_field'] = '%s required!';