<?php
// Text
$_['text_success'] = 'Success: API session successfully started!';

// Error
$_['error_key']    = 'Incorrect API Key!';
$_['error_ip']     = 'Your IP %s is not allowed to access this API!';