<?php
// Heading
$_['heading_title']      = 'My Account Information';

// Text
$_['text_account']       = 'Account';
$_['text_edit']          = 'Edit Information';
$_['text_your_details']  = 'Your Personal Details';
$_['text_success']       = 'Success: Your account has been successfully updated.';

// Entry
$_['entry_firstname']    = 'First Name';
$_['entry_lastname']     = 'Last Name';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Mobile number';

// Error
$_['error_exists']       = 'E-Mail address is already registered!';
$_['error_firstname']    = 'Name must be between 1 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 1 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']    = 'Mobile number should be a valid Indian number of 10 digits!';
$_['error_custom_field'] = '%s required!';
$_['error_telephone_exists']         = 'Mobile number is already registered!';