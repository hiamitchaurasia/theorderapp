<?php
// Heading
$_['heading_title'] = 'Your Account Has Been Created!';

// Text
$_['text_success']  = '<p>Congratulations! Your new account has been successfully created!</p> <p>You can now take advantage of member privileges to enhance your online shopping experience with us.</p> <p>If you have ANY questions about the operation of this online shop, please e-mail the store owner. please <a href="%s">contact us</a>.</p>';

$_['text_approval'] = '<p>Thank you for registering with %s!</p><p> We have sent an email with verification link, please verify your account to start shopping with us.</p><p> If you have ANY questions about the operation of this online shop, please <a href="%s">contact the store owner</a>.</p>';
$_['text_account']  = 'Account';