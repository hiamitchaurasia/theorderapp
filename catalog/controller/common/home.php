<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

 		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['tawko'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/home', $data));
	}

	public function getFacebookRedirectUrl(){
		
		$url = null;
		if(isset($this->request->get['category'])){
            $url = $this->request->get['category'];
        }

        if(isset($this->request->get['redirect_url'])) {

            if (($pos = strpos($this->request->get['redirect_url'], "route=")) !== FALSE) { 
                $redirectPath = substr($this->request->get['redirect_url'], $pos+5); 
                if($url) {
                    $redirectPath .='&category='.$url;
                }
                $this->session->data['redirect'] = $this->url->link($redirectPath);
            } else {
                $this->session->data['redirect'] = $this->request->get['redirect_url'];
            }
        }

       require DIR_SYSTEM . 'library/Facebook/autoload.php';

		$fb = new Facebook\Facebook( [
			'app_id' => !empty($this->config->get('config_fb_app_id')) ? $this->config->get('config_fb_app_id') : 'randomstringforappid',
			'app_secret' => !empty($this->config->get('config_fb_secret'))? $this->config->get('config_fb_secret') : 'randomstringforappsecret',
			'default_graph_version' => 'v2.5',
			//'default_access_token' => $this->request->get['code']//'5ce6c3df96acc19c6215f2ac62d3480e', // optional
			] );

		$helper = $fb->getRedirectLoginHelper();

		if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

		$json['facebook'] = $helper->getLoginUrl( $server . 'index.php?route=account/facebook', array( 'email' ) );

		echo json_encode($json);
	}

}